import inspect
import traceback
from collections import namedtuple


Task = namedtuple('Task', 'coroutine depends_on to_fulfill')


class Future:
    def __init__(self):
        self.state = 'pending'
        self._value = None

    def is_complete(self):
        return self.state == 'complete'

    @property
    def value(self):
        if not self.is_complete():
            raise RuntimeError('This future is still pending')
        return self._value

    @value.setter
    def value(self, x):
        if self.is_complete():
            raise RuntimeError('This future is already complete')
        self._value = x
        self.state = 'complete'


def run_coroutine(coro):
    """
    Runs given coroutine (as generator) and controls all spawned
    coroutines until all are done one way or another. It simply does a
    round-robin on the list of pending tasks until none is left.

    There are known bugs in this implementation but it should be
    simple enough to understand the most basic reasoning behind how an
    event loop works.

    Passing a regular function or value to this will probably break
    the execution. Please only pass generators to this.
    """

    # A noop future is a Null Object to be used whenever
    # a task with no dependencies is added to the list.
    # It exists only to free us from doing None checks
    # and keep the code a little bit tidier
    noop = Future()
    noop.value = None

    tasks = [Task(
        coroutine=coro,
        depends_on=noop,
        to_fulfill=None
    )]

    while tasks:
        queue, tasks = tasks, []

        for coro, depends_on, to_fulfill in queue:
            if not depends_on.is_complete():
                continue

            try:
                value = coro.send(depends_on.value)
            except StopIteration:
                pass
            except Exception:
                traceback.print_exc()
            else:
                if inspect.isgenerator(value):
                    future = Future()
                    tasks.append(Task(
                        coroutine=value,
                        depends_on=noop,
                        to_fulfill=future
                    ))
                    tasks.append(Task(
                        coroutine=coro,
                        depends_on=future,
                        to_fulfill=to_fulfill
                    ))
                elif to_fulfill:
                    to_fulfill.value = value
                else:
                    tasks.append(Task(
                        coroutine=coro,
                        depends_on=noop,
                        to_fulfill=None
                    ))
